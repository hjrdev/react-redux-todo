import { combineReducers } from 'redux';

import todos from './todos';

const index = combineReducers({
  todos,
});

export default index;
